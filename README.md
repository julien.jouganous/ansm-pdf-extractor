# PDF extractor

Extracts training data from ANSM pdf pharmacovigilance cases.
Works for python3.8

Install
-------

    pip install -r requirements.txt


Run code
--------

    python form_extraction.py --input folder_containing_your_pdf_files --output output.json --nb_proc 4


Help
----
	
	python form_exctraction.py -h
