#!/usr/bin/env python
"""
    ANSM PV pdf processing.

Usage:
  form_extraction.py [-i=<folder_name>] [-o=<output_file>] [-n=<nb_proc>] [-a=<use_hash]
  form_extraction.py [--input=<folder_name>] [--output=<output_file>] [--nb_proc=<nb_proc>] [--hash=<use_hash>]
  form_extraction.py -h

Options:
  -h --help       Getting help
  -i | --input=<folder_name>  path to input folder [default: input]
  -o | --output=<output_file>  path to output file [default: output.json]
  -n | --nb_proc=<nb_proc>      number of processors for parallel run [default: 1]
  -a | --hash=<use_hash>       anonymize filename using hash function [default: 0]
"""
import os
import json
from multiprocessing import Pool
import re
from docopt import docopt
from unidecode import unidecode
import pandas as pd
import hashlib
import camelot
from tqdm import tqdm


BLACK_LIST = [
    "ADDICTOVIGILANCE",
    "MATERIOVIGILANCE",
    "ERREUR_MEDICAMENTEUSE",
    "PROFESSIONNEL_SANTE",
]


def compute_hash(text):
    """
        text hash.
    """
    return hashlib.sha256(text.encode('utf-8')).hexdigest()


def read_table(filepath):
    tables = camelot.read_pdf(filepath, pages='all')
    df = pd.concat([t.df for t in tables],
                   ignore_index=True)
    df.columns = ['key', 'value']
    return filepath, df


def extract_department(field: str):
    cp = re.findall(r'\d{5}', field)
    if cp:
        return cp[0][:2]


def repare_dict(dictionnary):
    to_fix = []
    for k, v in dictionnary.items():
        if '\n' in k and not v:
            to_fix.append(k)
    for key in to_fix:
        del dictionnary[key]
        dictionnary[key.split('\n')[0]] = key.split('\n')[1]


def get_section(table, regex1, regex2):
    # Extracts sub-table between 2 given titles (regex)
    to_add = False
    if not regex1:
        to_add = True
    res = {}
    for row in table.iterrows():
        if to_add:
            res[unidecode(row[1]["key"].lower())] = row[1]["value"]
        if not to_add and re.findall(regex1, unidecode(row[1]["key"].lower())):
            to_add = True
        if re.findall(regex2, unidecode(row[1]["key"].lower())):
            break
    repare_dict(res)
    return res


def get_drug_section(table):
    # Extracts drugs (regex)
    to_add = False
    drugs = []
    res = {}
    for row in table.iterrows():
        if to_add:
            key = unidecode(row[1]["key"].lower())
            if re.findall(r".*edicament.*concern.*", key):
                drugs.append(res)
                res = {}
            res[unidecode(row[1]["key"].lower())] = row[1]["value"]
        if not to_add and re.findall(r".*edicament.*concern.*", unidecode(row[1]["key"].lower())):
            to_add = True
        if re.findall(r".*escription.*effet", unidecode(row[1]["key"].lower())):
            break
    if res:
        repare_dict(res)
        drugs.append(res)
    return drugs


def parse_drug(section):
    is_vaccine = regex_dict_get(section, r'agit.*vaccin')
    if is_vaccine and is_vaccine.lower() == "oui":
        is_vaccine = True
    else:
        is_vaccine = False
    indication = regex_dict_get(section, r'motif')
    if not indication:
        indication_key = [r for r in section.keys() if re.findall(r".*otif.*utilisation.*medicament", r)]
        if indication_key:
            indication_key = indication_key[0]
            if "\n" in indication_key:
                indication = indication_key.split('\n')[1]

    duration = regex_dict_get(section, r'duree')
    if not duration:
        duration_key = [r for r in section.keys() if re.findall(r".*duree", r)]
        if duration_key:
            duration_key = duration_key[0]
            if "\n" in duration_key:
                duration = duration_key.split('\n')[1]
    dose = regex_dict_get(section, r'dose')
    if not dose:
        dose = regex_dict_get(section, r'osologie')
    drug = {
        "label": regex_dict_get(section, r'medicament'),
        "is_vaccine": is_vaccine,
        "batch_number": regex_dict_get(section, r'de.*lot'),
        "administration_route": regex_dict_get(section, r'mode.*utilisation'),
        "dose": dose,
        "start_dt": regex_dict_get(section, r'debut.*utilisation'),
        "end_dt": regex_dict_get(section, r'fin.*utilisation'),
        "duration": duration,
        "indication": indication
    }
    return drug


def get_effects_sections(table):
    # Extracts sub-table between 2 given titles (regex)
    to_add = False
    res = []
    tmp = {}
    is_new_format = False
    for row in table.iterrows():
        if re.findall(r".*escription.*effet\s*\d", unidecode(row[1]["key"].lower())):
            is_new_format = True
        if to_add and not re.findall(r".*escription.*effet\s*\d", unidecode(row[1]["key"].lower())):
            tmp[unidecode(row[1]["key"].lower())] = row[1]["value"]
        if not to_add and re.findall(r".*escription.*effet\s*\d", unidecode(row[1]["key"].lower())):
            to_add = True
        if to_add and re.findall(r".*escription.*effet\s*\d", unidecode(row[1]["key"].lower())):
            repare_dict(tmp)
            res.append(tmp)
            tmp = {}
        if re.findall(r".*oordonnees.*professionnel.*sante.*", unidecode(row[1]["key"].lower())):
            repare_dict(tmp)
            res.append(tmp)
            break
    if not is_new_format:
        return [get_section(table, r".*escription.*effet", r".*oordonnees.*professionnel.*sante.*")]
    return [r for r in res if r]


def regex_dict_get(d, regex):
    # get dict value through fuzzy (regex) match on the key
    for k, v in d.items():
        if re.findall(unidecode(regex), unidecode(k)):
            return v


def parse_form(table):
    # infos du declarant
    section = get_section(table, None, r".*formation.*personne.*(present.*effet|expose).*")
    firstname = section["prenom"]
    name = section["nom"]
    initials = ''
    if firstname:
        initials += firstname[0]
    if name:
        initials += name[0]

    decl = {
        "initials": (initials).upper(),
        "department_num": extract_department(section["code postal / commune"])
    }

    is_same_person = regex_dict_get(section, r".*tes.*personne.*presente.*")
    if is_same_person:
        is_same_person = is_same_person.lower() == "oui"
    else:
        is_same_person = False
    # infos patient
    section = get_section(table, r".*formation.*personne.*(present.*effet|expose).*", r".*edicament.*concern.*")
    if is_same_person:
        patient = decl.copy()
    else:
        patient = {
            "initials": (regex_dict_get(section, r"nom")[0] +
                         regex_dict_get(section, r"prenom")[0]).upper(),
            "department_num": extract_department(regex_dict_get(section, r"code postal"))
        }
    patient.update(
        {
            "sex": section['sexe'],
            "birthdate": regex_dict_get(section, r".*naissance.*"),
            "med_background": regex_dict_get(section, r".*antecedents.*"),
            "weight": regex_dict_get(section, r"poids"),
            "height": regex_dict_get(section, r"taille")
        }
    )
    # medicaments
    drug_section = get_drug_section(table)
    drugs = [parse_drug(d) for d in drug_section]
    # date de l'effet
    section = get_section(table, r".*ate.*survenu.*effet.*", r".*escription.*effet")
    if not section:
        section = get_section(table, r".*escription.*effet", r".*oordonnees.*professionnel.*sante.*")
    date_ae = {
        "dt_event": regex_dict_get(section, r"date.*survenue"),
        "dt_approx": regex_dict_get(section, r"approximati"),
        "duration_event": regex_dict_get(section, r"duree.*effet")
    }

    # description de l'effet
    sections = get_effects_sections(table)
    descriptions = []
    for section in sections:
        descriptions.append(
            {
                "description": regex_dict_get(section, r"description"),
                "dt_event": regex_dict_get(section, r"ate.*survenue"),
                "evolution": regex_dict_get(section, r"evolution"),
                "other_situation": regex_dict_get(section, r"autre.*situation"),
                "consequences": regex_dict_get(section, r"consequences"),
                "consequences_details": regex_dict_get(section, r".*oui.*preciser"),
                "medical_care": regex_dict_get(section, r'prise en charge'),
                "severity": regex_dict_get(section, r"^gravite")
            }
        )

    result = {
        "declaring": decl,
        "patient": patient,
        "drugs": drugs,
        "date_event": date_ae,
        "description_event": descriptions
    }
    return result


def process_pdf_file(path_to_file: str):
    try:
        # 1 - we read the pdf
        filepath, df = read_table(path_to_file)
        # 2 - we structure the extracted data

        document = parse_form(df)

    except: # avoid breaking if a doc is not formatted as expected
        document = "PARSING_ERROR"
        filepath = path_to_file

    return {filepath: document}


def run_imap_unordered_multiprocessing(func, argument_list, num_processes):

    pool = Pool(processes=num_processes)

    result_list_tqdm = []
    for result in tqdm(pool.imap_unordered(func=func, iterable=argument_list), total=len(argument_list)):
        result_list_tqdm.append(result)

    return result_list_tqdm


def in_black_list(filename: str, black_list: str = BLACK_LIST):
    for s in black_list:
        if s.lower() in filename.lower():
            return True
    return False


def process_folder(input_folder: str, output_file: str, nb_process: int = 1, use_hash: int = 0):
    nb_process = int(nb_process)
    use_hash = int(use_hash)
    print("Discovering files from {} repository...".format(input_folder))
    # 1 - list all pdf files in folder
    pdf_files = [
        os.path.join(input_folder, f) for f in os.listdir(input_folder) if
        os.path.isfile(os.path.join(input_folder, f)) and
        f.endswith('.pdf')
    ]
    print("{} pdf files discovered.".format(len(pdf_files)))
    # filtering
    pdf_files = [filename for filename in pdf_files if not in_black_list(filename)]

    print("{} valid files kept.".format(len(pdf_files)))

    # 2 - files processing
    print("Processing files...")
    if nb_process > 1:
        results = run_imap_unordered_multiprocessing(process_pdf_file, pdf_files, nb_process)
    else:
        results = []
        for file in tqdm(pdf_files):
            results.append(process_pdf_file(file))

    # 3 - generating a single json file
    if use_hash:
        json_result = {
            compute_hash(k): v for dictionnary in results for k, v in dictionnary.items()
        }
    else:
        json_result = {
            k: v for dictionnary in results for k, v in dictionnary.items()
        }

    # 4 - results export
    print("Expotring results in {} file.".format(output_file))
    with open(output_file, 'w') as outfile:
        json.dump(json_result, outfile)
    print("Done")


if __name__ == "__main__":
    # we get config file from command line
    arguments = docopt(__doc__)
    folder_name = arguments.get("--input")
    use_hash = arguments.get("--hash")
    output_file = arguments.get("--output")
    nb_proc = arguments.get("--nb_proc")

    _ = process_folder(folder_name, output_file, nb_process=nb_proc, use_hash=use_hash)
